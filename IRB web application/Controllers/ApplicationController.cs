﻿ using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    public class ApplicationController : Controller
    {

        ApplicationDbContext context;
        private ApplicationUserManager _userManager;

        public ApplicationController()
        {
            context = new ApplicationDbContext();
        }
        public ApplicationController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ContinueApplication()
        {
            var user = User.Identity;

            var nonCompletedApplicationsList = context.ApplicationBasicDetails.ToList().Where(c => c.UserID == user.GetUserId() & c.isSubmitted == false);
            return View(nonCompletedApplicationsList);
        }

        // GET: ApplicationBasicDetail
        public ActionResult BasicDetail()
        {
            //ViewBag.PI = context.Users.Find(User.Identity.GetUserId());
            ViewBag.ApplicationTypeList = new SelectList(new List<string> { "Expedited", "Standard", "Exemption from Standard Review" });
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            ApplicationUser PI = new ApplicationUser();
            PI = context.Users.Find(User.Identity.GetUserId());
            ViewBag.PIFullName = PI.FirstName + " "+ PI.MiddleName +" " + PI.LastName;
            ViewBag.PIMailingAddress = PI.StreetAddress1 +" "+ PI.City +", "+ PI.State +" "+ PI.Zip;
            ViewBag.PIPhoneNumber = PI.PhoneNumber;
            if (Session["ApplicationID"] != null)
            {
               
                applicationBasicDetail = context.ApplicationBasicDetails.Find(Session["ApplicationID"].ToString());
                

               

            }
            return View(applicationBasicDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BasicDetail(ApplicationBasicDetail applicationdetail)
        {
            //try
            //{
            //    ViewBag.ApplicationTypeList = new SelectList(new List<string> { "Expedited", "Standard", "Exemption from Standard Review" });

                if (Session["ApplicationID"] != null & Session["UserID"] != null)
                {
                    applicationdetail.ApplicationID = Session["ApplicationID"].ToString();
                    applicationdetail.UserID = Session["UserID"].ToString();
                }
            ViewBag.PI = context.Users.Find(User.Identity.GetUserId());
            var existingApplication = await context.ApplicationBasicDetails.FindAsync(applicationdetail.ApplicationID);
                if (ModelState.IsValid)
                {
                    var user = User.Identity;

                    if (existingApplication != null)
                    {
                        // exists, Update Application
                        existingApplication.isGraduateStudent = applicationdetail.isGraduateStudent;
                        existingApplication.PISignedInitial = applicationdetail.PISignedInitial;
                        existingApplication.TitleOfResearch = applicationdetail.TitleOfResearch;
                        existingApplication.TypeOfApplication = applicationdetail.TypeOfApplication;
                        if (existingApplication.TypeOfApplication != "Standard")
                        {
                            if (existingApplication.TypeOfApplication == "Expedited" || existingApplication.TypeOfApplication == "Exemption from Standard Review")
                            {
                                existingApplication.isRerResr = true;// True means it is Request For Expedited Review Or Request for Exemption from standard Review
                                if(existingApplication.TypeOfApplication == "Expedited")
                                {
                                    ViewBag.SelectedApplication = "Expedited";
                                }else if(existingApplication.TypeOfApplication == "Exemption from Standard Review")
                                {
                                    ViewBag.SelectedApplication = "Exemption from Standard Review";
                                }
                            }
                            else 
                            {
                                existingApplication.isRerResr = false;
                            }
                        }
                        context.Entry(existingApplication).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        //doesn't exist, save new Application
                        applicationdetail.ApplicationID = Guid.NewGuid().ToString() ;
                        applicationdetail.UserID = user.GetUserId();
                        if (applicationdetail.TypeOfApplication != "Standard")
                        {
                            if (applicationdetail.TypeOfApplication == "Expedited" || applicationdetail.TypeOfApplication == "Exemption from Standard Review")
                            {
                                applicationdetail.isRerResr = true;// True means it is Request For Expedited Review Or Request for Exemption from standard Review
                                
                                if (applicationdetail.TypeOfApplication == "Expedited")
                                {
                                    ViewBag.SelectedApplication = "Expedited";
                                }
                                else if (applicationdetail.TypeOfApplication == "Exemption from Standard Review")
                                {
                                    ViewBag.SelectedApplication = "Exemption from Standard Review";
                                }
                            }
                            else
                            {
                                applicationdetail.isRerResr = false;
                            }
                        }
                    if (User.IsInRole("Student"))
                    {
                        applicationdetail.isGraduateStudent = true;
                    }
                        //applicationdetail.ApplicationIDint = getNextApplicationID().AppID_int;
                        applicationdetail.LastEditedDate = DateTime.Now;
                        applicationdetail.ApplicationStartDate = DateTime.Now;
                        applicationdetail.isCompleted = true;
                        applicationdetail.ApplicationState = "Pending";
                        applicationdetail.DateStateUpdate = DateTime.Now;
                    context.ApplicationBasicDetails.Add(applicationdetail);
                        Session["ApplicationID"] = applicationdetail.ApplicationID;
                        Session["UserID"] = applicationdetail.UserID;

                    }


                    await context.SaveChangesAsync();

                }
            //}
            //catch (Exception e)
            //{

            //}
            if (applicationdetail.isGraduateStudent)
            {
                return RedirectToAction("FacultySponsor");
            }
           if (applicationdetail.TypeOfApplication == "Exemption from Standard Review") 
            {
                return RedirectToAction("ResrForm");
            }
            else if (applicationdetail.TypeOfApplication == "Expedited")
            {
                return RedirectToAction("RerForm");
            }
            else
            {
                return RedirectToAction("ResearchStudyDetail");
            }
            //return View();
        }
        

        public ActionResult ResearchStudyDetail()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "A new grant/contract", Value = "1" });
            items.Add(new SelectListItem { Text = "A dissertation or thesis", Value = "2" });
            items.Add(new SelectListItem { Text = "An independent study", Value = "3" });
            items.Add(new SelectListItem { Text = "A graduate research proposal", Value = "4" });
            items.Add(new SelectListItem { Text = "A class project", Value = "5" });
            items.Add(new SelectListItem { Text = "An undergraduate research proposal", Value = "6" });
            items.Add(new SelectListItem { Text = " A grant application", Value = "7" });

            //Need to Test This Action
            ResearchStudyDetail existingResearchStudyDetail = new ResearchStudyDetail();
            ResearchStudyDetailViewModel researchStudyDetailViewModel = new ResearchStudyDetailViewModel();
            researchStudyDetailViewModel.ResearchStudyProposal = items;
            ViewBag.PreviousUrl = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                existingResearchStudyDetail = context.ResearchStudyDetails.FirstOrDefault(c => c.ApplicationID == id);
                researchStudyDetailViewModel.ResearchStudyDetail = existingResearchStudyDetail;

            }
            return View(researchStudyDetailViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResearchStudyDetail(ResearchStudyDetailViewModel researchStudyDetail)
        {
            //try
            //{
            string ResearchDets = "";
                ViewBag.PreviousUrl = System.Web.HttpContext.Current.Request.UrlReferrer.ToString();
                if (Session["ApplicationID"] != null )
                {
                    researchStudyDetail.ResearchStudyDetail.ApplicationID = Session["ApplicationID"].ToString();
               
               
                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                if (ModelState.IsValid)
                {
                    var existingResearchStudyDetail = await context.ResearchStudyDetails.FindAsync(researchStudyDetail.ResearchStudyDetail.ApplicationID);
                    if (existingResearchStudyDetail != null)
                    {
                        existingResearchStudyDetail = researchStudyDetail.ResearchStudyDetail;
                        
                        existingResearchStudyDetail.LastEditedDate = DateTime.Now;
                        //update data in database 
                        context.Entry(existingResearchStudyDetail).State = System.Data.Entity.EntityState.Modified;

                    }
                    else
                    {
                    //doesn't exist, start new application
                        researchStudyDetail.ResearchStudyDetail.ID = Guid.NewGuid().ToString();
                        researchStudyDetail.ResearchStudyDetail.isCompleted = true;
                        researchStudyDetail.ResearchStudyDetail.LastEditedDate = DateTime.Now;
                        foreach(SelectListItem c in researchStudyDetail.ResearchStudyProposal)
                    {
                        ResearchDets += c.Text.ToString() + "<br>";
                    }
                        context.ResearchStudyDetails.Add(researchStudyDetail.ResearchStudyDetail);

                    }

                    await context.SaveChangesAsync();
                }
            //}
            //catch (Exception e)
            //{

            //}

            return RedirectToAction("SubjectDefinedDetail");
        }

        //RiskFactorDetail
        public ActionResult RiskFactorDetail()
        {
            RiskFactorDetail riskFactorDetail = new RiskFactorDetail();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                riskFactorDetail = context.RiskFactorDetails.FirstOrDefault(c => c.ApplicationID ==id);


            }
            return View(riskFactorDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RiskFactorDetail(RiskFactorDetail riskFactorDetail)
        {
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    riskFactorDetail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingRiskFactorDetail = await context.RiskFactorDetails.FindAsync(riskFactorDetail.ApplicationID);
                if (existingRiskFactorDetail != null)
                {
                    existingRiskFactorDetail = riskFactorDetail;                   
                    existingRiskFactorDetail.LastEditedDate = DateTime.Now;
                    //update data in database 
                    context.Entry(existingRiskFactorDetail).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    riskFactorDetail.ID = Guid.NewGuid().ToString();
                    riskFactorDetail.isCompleted = true;
                    riskFactorDetail.LastEditedDate = DateTime.Now;
                    context.RiskFactorDetails.Add(riskFactorDetail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("InformedConsent1");
        }


        //DataHandlingDetail
        public ActionResult DataHandlingDetail()
        {
            DataHandlingDetail dataHandlingDetail = new DataHandlingDetail();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                dataHandlingDetail = context.DataHandlingDetails.FirstOrDefault(c => c.ApplicationID == id);


            }
            return View(dataHandlingDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DataHandlingDetail(DataHandlingDetail dataHandlingDetail)
        {
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    dataHandlingDetail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingDataHandlingDetail = await context.DataHandlingDetails.FindAsync(dataHandlingDetail.ApplicationID);
                if (existingDataHandlingDetail != null)
                {
                    existingDataHandlingDetail = dataHandlingDetail;
                    
                    existingDataHandlingDetail.LastEditedDate = DateTime.Now;
                    //update data in database 
                    context.Entry(existingDataHandlingDetail).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    dataHandlingDetail.ID = Guid.NewGuid().ToString();
                    dataHandlingDetail.isCompleted = true;
                    dataHandlingDetail.LastEditedDate = DateTime.Now;
                    context.DataHandlingDetails.Add(dataHandlingDetail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("SubjectDefinedDetail");
        }

        //SubjectDefinedDetail
        public ActionResult SubjectDefinedDetail()
        {
            SubjectDefinedDetail subjectDefinedDetail = new SubjectDefinedDetail();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                subjectDefinedDetail = context.SubjectDefinedDetails.FirstOrDefault(c => c.ApplicationID == id);


            }
            return View(subjectDefinedDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SubjectDefinedDetail(SubjectDefinedDetail subjectDefinedDetail)
        {
            
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    subjectDefinedDetail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingSubjectDefinedDetail = await context.SubjectDefinedDetails.FindAsync(subjectDefinedDetail.ApplicationID);
                if (existingSubjectDefinedDetail != null)
                {

                    existingSubjectDefinedDetail = subjectDefinedDetail;
                    existingSubjectDefinedDetail.LastEditedDate = DateTime.Now;
                    
                    //update data in database 
                    context.Entry(existingSubjectDefinedDetail).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    subjectDefinedDetail.ID = Guid.NewGuid().ToString();
                    subjectDefinedDetail.isCompleted = true;
                    subjectDefinedDetail.LastEditedDate = DateTime.Now;
                    
                    context.SubjectDefinedDetails.Add(subjectDefinedDetail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("RiskFactorDetail");
        }

        //RerResr
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  RerResr RerResr(RerResr RerResrdetail)
        {
            RerResr Object = new RerResr();
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    RerResrdetail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return null;
                }
                var existingRerResr =  context.RerResrs.Find(RerResrdetail.ApplicationID);
                if (existingRerResr != null)
                {

                    existingRerResr = RerResrdetail;
                    existingRerResr.LastEditedDate = DateTime.Now;
                    Object = existingRerResr;
                    //update data in database 
                    context.Entry(existingRerResr).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    RerResrdetail.ID = Guid.NewGuid().ToString();
                    RerResrdetail.isCompleted = true;
                    RerResrdetail.LastEditedDate = DateTime.Now;
                    Object = RerResrdetail;
                    context.RerResrs.Add(RerResrdetail);

                }

                 context.SaveChanges();
            //}
            //catch (Exception e)
            //{

            //}
            return Object;
        }

        public ActionResult RerForm()
        {
            RerResr detail = new RerResr();
            
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                detail = context.RerResrs.FirstOrDefault(c => c.ApplicationID == id);


            }
            return View(detail);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult RerForm(RerResr RerDetail)
        {
            var detail =  RerResr(RerDetail);
            if (detail == null)
            {
                return View(detail);
            }
            return RedirectToAction("ResearchStudyDetail");
        }

        public ActionResult ResrForm()
        {
            RerResr detail = new RerResr();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                detail = context.RerResrs.FirstOrDefault(c => c.ApplicationID == id);


            }
            return View(detail);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult ResrForm(RerResr ResrDetail)
        {
            var detail =  RerResr(ResrDetail);
            if (detail == null)
            {
                return View(detail);
            }
            return RedirectToAction("ResearchStudyDetail");
        }

        //InformedConsent1
        public ActionResult InformedConsent1()
        {
            InformedConsent1 informedConsent1 = new InformedConsent1();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                informedConsent1 = context.InformedConsent1s.FirstOrDefault(c => c.ApplicationID == id);

            }
            return View(informedConsent1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InformedConsent1(InformedConsent1 InformedConsent1detail)
        {
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    InformedConsent1detail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingInformedConsent1 = await context.InformedConsent1s.FindAsync(InformedConsent1detail.ApplicationID);
                if (existingInformedConsent1 != null)
                {

                    existingInformedConsent1 = InformedConsent1detail;
                    existingInformedConsent1.LastEditedDate = DateTime.Now;
                    //update data in database 
                    context.Entry(existingInformedConsent1).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    InformedConsent1detail.ID = Guid.NewGuid().ToString();
                    InformedConsent1detail.isCompleted = true;
                    InformedConsent1detail.LastEditedDate = DateTime.Now;
                    context.InformedConsent1s.Add(InformedConsent1detail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("InformedConsent2");
        }


        //InformedConsent2
        public ActionResult InformedConsent2()
        {
            InformedConsent2 informedConsent2 = new InformedConsent2();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                informedConsent2 = context.InformedConsent2s.FirstOrDefault(c => c.ApplicationID == id);

            }
            return View(informedConsent2);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InformedConsent2(InformedConsent2 InformedConsent2detail)
        {
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    InformedConsent2detail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingInformedConsent2 = await context.InformedConsent2s.FindAsync(InformedConsent2detail.ApplicationID);
                if (existingInformedConsent2 != null)
                {

                    existingInformedConsent2 = InformedConsent2detail;
                    existingInformedConsent2.LastEditedDate = DateTime.Now;
                    //update data in database 
                    context.Entry(existingInformedConsent2).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    InformedConsent2detail.ID = Guid.NewGuid().ToString();
                    InformedConsent2detail.isCompleted = true;
                    InformedConsent2detail.LastEditedDate = DateTime.Now;
                    context.InformedConsent2s.Add(InformedConsent2detail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("InformedConsent3");
        }

        //InformedConsent3
        public ActionResult InformedConsent3()
        {
            InformedConsent3 informedConsent3 = new InformedConsent3();
            if (Session["ApplicationID"] != null)
            {
                string id = Session["ApplicationID"].ToString();
                informedConsent3 = context.InformedConsent3s.FirstOrDefault(c => c.ApplicationID == id);

            }
            return View(informedConsent3);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InformedConsent3(InformedConsent3 InformedConsent3detail)
        {
            //try
            //{
                if (Session["ApplicationID"] != null)
                {
                    InformedConsent3detail.ApplicationID = Session["ApplicationID"].ToString();

                }
                else
                {
                    return RedirectToAction("BasicDetail");
                }
                var existingInformedConsent3 = await context.InformedConsent3s.FindAsync(InformedConsent3detail.ApplicationID);
                if (existingInformedConsent3 != null)
                {

                    existingInformedConsent3 = InformedConsent3detail;
                  
                    existingInformedConsent3.LastEditedDate = DateTime.Now;
                    //update data in database 
                    context.Entry(existingInformedConsent3).State = System.Data.Entity.EntityState.Modified;

                }
                else
                {
                    //doesn't exist, start new application
                    InformedConsent3detail.ID = Guid.NewGuid().ToString();
                    InformedConsent3detail.isCompleted = true;
                    InformedConsent3detail.LastEditedDate = DateTime.Now;
                    context.InformedConsent3s.Add(InformedConsent3detail);

                }

                await context.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //}
            return RedirectToAction("UploadFiles");
        }

        public ActionResult Preview(string id)
        {
            string ApplicationID = id;
            if(id == null)
                ApplicationID = Session["ApplicationID"].ToString();


            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var PI = applicationBasicDetail.GetPrincipalInvestigator(ApplicationID);
            ApplicationViewModel viewModel = new ApplicationViewModel
            {
                ApplicationBasicDetail = context.ApplicationBasicDetails.Find(ApplicationID),
                RerResr = context.RerResrs.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                ResearchStudyDetail = context.ResearchStudyDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                SubjectDefinedDetail = context.SubjectDefinedDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                RiskFactorDetail = context.RiskFactorDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent1 = context.InformedConsent1s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent2 = context.InformedConsent2s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent3 = context.InformedConsent3s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                PrincipalInvestigator = PI,
               

            };
            return View(viewModel);
        }

       


        [HttpPost]      
        public ActionResult Submit()
        {
            string AppID = Session["ApplicationID"].ToString();
            
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();

               
            applicationBasicDetail = context.ApplicationBasicDetails.Find(AppID);
            ApplicationUser PI = context.Users.Find(applicationBasicDetail.UserID);

            if(User.IsInRole("Student") || applicationBasicDetail.isGraduateStudent)
            {
                if (!applicationBasicDetail.isFacultyApproved)
                {
                    return RedirectToAction("Preview", new { Id = AppID });
                }
            }
            applicationBasicDetail.SubmittedApplicationID = getNextApplicationID().newID;
            applicationBasicDetail.ApplicationIDint = getNextApplicationID().AppID_int;
            applicationBasicDetail.DateSubmitted = DateTime.Now;
            applicationBasicDetail.isSubmitted = true;
            applicationBasicDetail.ApplicationState = "Submitted";
            applicationBasicDetail.DateStateUpdate = DateTime.Now;
            context.Entry(applicationBasicDetail).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
            
            return RedirectToAction("Index", "Application");
        }

        public ActionResult SumbittedApplication()
        {
            var user = User.Identity;
           

            var CompletedApplicationsList = context.ApplicationBasicDetails.ToList().Where(c => c.UserID == user.GetUserId() & c.isSubmitted == true);
            return View(CompletedApplicationsList);
        }

       

        public async Task<ActionResult> EditApplication(string id)
        {
            Session["ApplicationID"] = id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationBasicDetail applicationBasicDetail = await context.ApplicationBasicDetails.FindAsync(id);
            if (applicationBasicDetail.isCompleted)
            {
                if(applicationBasicDetail.TypeOfApplication != "Standard")
                {
                    RerResr rerResr = new RerResr();
                    if (!rerResr.GetisCompleted(id))
                    {
                        if (applicationBasicDetail.TypeOfApplication == "Expedited")
                        {
                            return RedirectToAction("RerForm");
                        }
                        else
                        {
                            return RedirectToAction("ResrForm");
                        }
                    }
                }

                ResearchStudyDetail researchStudyDetail = new ResearchStudyDetail();
                if (!researchStudyDetail.GetisCompleted(id))
                {
                    return RedirectToAction("ResearchStudyDetail");
                }

                SubjectDefinedDetail subjectDefinedDetail = new SubjectDefinedDetail();
                if (!subjectDefinedDetail.GetisCompleted(id))
                {
                    return RedirectToAction("SubjectDefinedDetail");
                }

                RiskFactorDetail riskFactorDetail = new RiskFactorDetail();
                if (!riskFactorDetail.GetisCompleted(id))
                {
                    return RedirectToAction("RiskFactorDetail");
                }

                InformedConsent1 informedConsent1 = new InformedConsent1();
                if (!informedConsent1.GetisCompleted(id))
                {
                    return RedirectToAction("InformedConsent1");
                }

                InformedConsent2 informedConsent2 = new InformedConsent2();
                if (!informedConsent2.GetisCompleted(id))
                {
                    return RedirectToAction("InformedConsent2");
                }

                InformedConsent3 informedConsent3 = new InformedConsent3();
                if (!informedConsent3.GetisCompleted(id))
                {
                    return RedirectToAction("InformedConsent3");
                }
                //to Upload Files 

                return RedirectToAction("UploadFiles");

            }
            else
            {
                return RedirectToAction("BasicDetail");
            }
            
        }

        public ActionResult CollaborativeUsersManagement()
        {
            string AppID = Session["ApplicationID"].ToString();
            foreach(var user in GetCollaborativeUsers(AppID))
            {
                if(user.isRegistered == true)
                {
                    ApplicationUser RegisteredUser = new ApplicationUser();
                    RegisteredUser = context.Users.FirstOrDefault(c => c.Email == user.EmailAddress);
                    ViewBag.ExistingUser = RegisteredUser;
                }
            }

            return View(GetCollaborativeUsers(AppID));
        }

        [HttpPost]
        public async Task<ActionResult> CollaborativeUsersManagement(CollaborativeUsers collaborativeUsers)
        {
            string AppID = Session["ApplicationID"].ToString();
            if (ModelState.IsValid)
            {
                CollaborativeUsers UserT = new CollaborativeUsers();
                UserT.ID = Guid.NewGuid().ToString();
                UserT.ApplicationID = AppID;
                UserT.EmailAddress = collaborativeUsers.EmailAddress;
                UserT.FullName = collaborativeUsers.FullName;

                if(context.Users.FirstOrDefault(c => c.Email == UserT.EmailAddress) != null)
                {
                    ApplicationUser PresentUser = context.Users.FirstOrDefault(c => c.Email == UserT.EmailAddress);
                    UserT.isRegistered = true;
                    UserT.IRBUserID = PresentUser.Id;
                }
                else
                {
                    Session["NewUserEmail"] = UserT.EmailAddress;
                    ApplicationBasicDetail ResearchApp = context.ApplicationBasicDetails.Find(AppID);
                    string to = UserT.EmailAddress;
                    string body = "Hello "+ UserT.FullName + ",<br/> You have been added as a collaborative user by "+ User.Identity.GetUserName()+" for the Research work Titled: "+ ResearchApp.TitleOfResearch+".<br/>" +
                        "To confirm this invitation, follow the link to register and activate your account. http://localhost:63117/Account/Register";
                    string subject = "AdminIRB: Collaborative Users Management System";
                    await this.SendEmail(to, body, subject);
                }
                if (UserT.isRegistered == true)
                {
                    ApplicationBasicDetail ResearchApp = context.ApplicationBasicDetails.Find(AppID);
                    ApplicationUser RegisteredUser = new ApplicationUser();
                    RegisteredUser = context.Users.FirstOrDefault(c => c.Email == UserT.EmailAddress);
                    string to = UserT.EmailAddress;
                    string body = "Hello " + RegisteredUser.FirstName + ",<br/> You have been added as a collaborative user by " + User.Identity.GetUserName() + " for the Research work Titled: " + ResearchApp.TitleOfResearch + ".<br/>" +
                        "To View this Application, follow the link. http://localhost:63117/CollaborativeUsersManagement";
                    string subject = "AdminIRB: Collaborative Users Management System";
                    await this.SendEmail(to, body, subject);
                    ViewBag.ExistingUser = RegisteredUser;
                }

                context.CollaborativeUsers.Add(UserT);
                context.SaveChanges();

            }
            return View(GetCollaborativeUsers(AppID));
        }

        public List<CollaborativeUsers> GetCollaborativeUsers(string Id)
        {
            
            CollaborativeUsers users = new CollaborativeUsers();
            return users.GetCollabUsers(Id);
        }

        [HttpPost]
        public ActionResult DeleteCollabUser(string fileId)
        {
            CollaborativeUsers User = new CollaborativeUsers();
            User = context.CollaborativeUsers.Find(fileId);
            context.CollaborativeUsers.Remove(User);
            context.SaveChanges();
            return RedirectToAction("CollaborativeUsersManagement");
        }

       //Not working 
        public PartialViewResult TrackApplication()
        {
            string AppID = Session["ApplicationID"].ToString();
           
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var PI = applicationBasicDetail.GetPrincipalInvestigator(AppID);


            ApplicationViewModel Application = new ApplicationViewModel
            {
                ApplicationBasicDetail = context.ApplicationBasicDetails.Find(AppID),
                RerResr = context.RerResrs.FirstOrDefault(c => c.ApplicationID == AppID),
                ResearchStudyDetail = context.ResearchStudyDetails.FirstOrDefault(c => c.ApplicationID == AppID),
                SubjectDefinedDetail = context.SubjectDefinedDetails.FirstOrDefault(c => c.ApplicationID == AppID),
                RiskFactorDetail = context.RiskFactorDetails.FirstOrDefault(c => c.ApplicationID == AppID),
                InformedConsent1 = context.InformedConsent1s.FirstOrDefault(c => c.ApplicationID == AppID),
                InformedConsent2 = context.InformedConsent2s.FirstOrDefault(c => c.ApplicationID == AppID),
                InformedConsent3 = context.InformedConsent3s.FirstOrDefault(c => c.ApplicationID == AppID),
                PrincipalInvestigator = PI,
                 

            };

            return PartialView("ApplicationTracker", Application);
        }

        public ActionResult UploadFiles()
        {
              string AppID = Session["ApplicationID"].ToString();
            UploadLinksViewModel uploadLinksViewModel = new UploadLinksViewModel();
            
            
            List<Hyperlinks> hyperlinksList = new List<Hyperlinks>();
            hyperlinksList = GetHyperlinks(AppID);
            if (hyperlinksList != null)
            {
                uploadLinksViewModel.Hyperlinks = hyperlinksList;
            }
            uploadLinksViewModel.uploadFiles = GetFiles(AppID);
            return View(uploadLinksViewModel);
        }


        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase postedFile)
        {
            string AppID = Session["ApplicationID"].ToString();
            UploadLinksViewModel uploadLinksViewModel = new UploadLinksViewModel();
            
            UploadFiles upload = new UploadFiles();
           
            byte[] bytes;
            using (BinaryReader br = new BinaryReader(postedFile.InputStream))
            {
                bytes = br.ReadBytes(postedFile.ContentLength);
            }

            upload.ID = Guid.NewGuid().ToString();
            upload.ApplicationID = AppID;
            upload.Name = postedFile.FileName;
            upload.ContentType = postedFile.ContentType;
            upload.Data = bytes;
            upload.LastEditedDate = DateTime.Now;
            context.UploadFiles.Add(upload);
            context.SaveChanges();


            List<Hyperlinks> hyperlinksList = GetHyperlinks(AppID);
            if(hyperlinksList.Count > 0)
            {
                uploadLinksViewModel.Hyperlinks = hyperlinksList;
            }
            uploadLinksViewModel.uploadFiles = GetFiles(AppID);
            return View(uploadLinksViewModel);
        }

        [HttpPost]
        public ActionResult PostLink(Hyperlinks hyperlinks)
        {
            string AppID = Session["ApplicationID"].ToString();
            Hyperlinks Links = new Hyperlinks();
            Links.ID = Guid.NewGuid().ToString();
            Links.ApplicationID = AppID;
            Links.Name = hyperlinks.Name;
            Links.Link = hyperlinks.Link;
            Links.Description = hyperlinks.Description;
            context.Hyperlinks.Add(Links);
            context.SaveChanges();

            return RedirectToAction("UploadFiles");
        }

        
        public ActionResult DeleteLink(string fileId)
        {
            
            Hyperlinks hyperlinks = new Hyperlinks();
            hyperlinks = context.Hyperlinks.Find(fileId);
            context.Hyperlinks.Remove(hyperlinks);
            context.SaveChanges();

            return RedirectToAction("UploadFiles");
        }


        


        public List<Hyperlinks> GetHyperlinks(string Id)
        {
             
            Hyperlinks hype = new Hyperlinks();
            List<Hyperlinks> links = hype.GetHyperlinks(Id);
            return links;
        }

        private static List<UploadFiles> GetFiles(string id)
        {
           
            UploadFiles file = new UploadFiles();
            List<UploadFiles> files = file.GetUploadedFiles(id);
            return files;
        }

        [HttpPost]
        public FileResult DownloadFile(string fileId)
        {
            byte[] bytes;
            string fileName, contentType;
            UploadFiles FileToDownload = new UploadFiles();
            FileToDownload = FileToDownload.GetFIleToDownload(fileId);
            fileName = FileToDownload.Name;
            contentType = FileToDownload.ContentType;
            bytes = FileToDownload.Data;                        
            return File(bytes, contentType, fileName);
        }

        public ActionResult DeleteFile(string fileId)
        {
            string AppID = Session["ApplicationID"].ToString();
            UploadFiles upload = new UploadFiles();
            upload = context.UploadFiles.Find(fileId);
            context.UploadFiles.Remove(upload);
            context.SaveChanges();
            return RedirectToAction("UploadFiles");
        }

        //use in the last submission procedure to generate SubmittedApplicationID
        public IdModel getNextApplicationID()
        {
            IdModel idModel = new IdModel();
            var query = context.ApplicationBasicDetails.OrderByDescending(p => p.ApplicationIDint);
                        

            int lastID = 0;
            if (query != null)
            {
                var App = query.FirstOrDefault();
                lastID = App.ApplicationIDint;
                idModel.AppID_int = lastID;
            }
            idModel.AppID_int += 1;
            string newID = (idModel.AppID_int).ToString();


            idModel.newID = "GUIRB" + "-" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + newID;
            return idModel;
        }

        public async Task SendEmail(string to, string body, string Subject)
        {
            GMailer mailer = new GMailer
            {
                ToEmail = to,
                Subject = Subject,
                Body = body,
                IsHtml = true
            };
            await mailer.Send();
        }

        public ActionResult FacultySponsor()
        {
            string AppID = Session["ApplicationID"].ToString();
            ApplicationBasicDetail BasicDetail = new ApplicationBasicDetail();
            BasicDetail = context.ApplicationBasicDetails.Find(AppID);
            return View(BasicDetail);
        }


        [HttpPost]
        public async Task<ActionResult> FacultySponsor(ApplicationBasicDetail applicationBasicDetail)
        {
            string AppID = Session["ApplicationID"].ToString();
            ApplicationBasicDetail BasicDetail = new ApplicationBasicDetail();
            BasicDetail = context.ApplicationBasicDetails.Find(AppID);
            BasicDetail.FacultyEmail = applicationBasicDetail.FacultyEmail;
            BasicDetail.FacultyName = applicationBasicDetail.FacultyName;
            BasicDetail.FacultyPhoneNumber = applicationBasicDetail.FacultyPhoneNumber;
            ApplicationUser facultyuser = context.Users.FirstOrDefault(c => c.Email == applicationBasicDetail.FacultyEmail);
            if (facultyuser != null)
            {
                BasicDetail.FacultyUserID = facultyuser.Id;
            }


            context.Entry(BasicDetail).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();

            string to = BasicDetail.FacultyEmail;
            string subject = "GannonIRB: New Faculty Sponsorship Request";
            string body = "Hello "+ BasicDetail.FacultyName+", <br/><br/>  The reason for this email is that a student has added you as a faculty sponsor for their" +
                "research work, the details are" +
                "<br/><br/>" +
                "Research Title: "+ BasicDetail.TitleOfResearch
                +"<br/>" +
                "PI: "+ BasicDetail.PrincipalInvestigator.FirstName +" "+ BasicDetail.PrincipalInvestigator.LastName
                +" <br/><br/>" +
                "";
            await this.SendEmail(to, body, subject); 
            return View(BasicDetail);

            

            
        }

        public async Task<ActionResult> RemindFaculty()
        {
            string AppID = Session["ApplicationID"].ToString();
            ApplicationBasicDetail BasicDetail = new ApplicationBasicDetail();
            BasicDetail = context.ApplicationBasicDetails.Find(AppID);
            string to = BasicDetail.FacultyEmail;
            string subject = "GannonIRB: Faculty Sponsorship Reminder";
            string body = "Hello " + BasicDetail.FacultyName + ", <br/><br/>  This is a Reminder for the Application with details below-" +
                "<br/><br/>" +
                "Research Title: " + BasicDetail.TitleOfResearch
                + "<br/>" +
                "PI: " + BasicDetail.PrincipalInvestigator.FirstName + " " + BasicDetail.PrincipalInvestigator.LastName
                + " <br/><br/>" +
                "";
            await this.SendEmail(to, body, subject);
            return RedirectToAction("FacultySponsor");
        }

        public class IdModel
        {
            public int AppID_int { get; set; }
            public string newID { get; set; }
        }

        // GET: ApplicationBasicDetail/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ApplicationBasicDetail/Save
        public ActionResult Save()
        {

            return View();
        }

        // POST: ApplicationBasicDetail/Save
        [HttpPost]
        public ActionResult Save(ApplicationBasicDetail BasicApp)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApplicationBasicDetail/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ApplicationBasicDetail/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ApplicationBasicDetail/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ApplicationBasicDetail/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
