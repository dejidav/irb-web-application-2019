﻿using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    public class FacultyHubController : Controller
    {

        ApplicationDbContext context;
        private ApplicationUserManager _userManager;
        
        public FacultyHubController()
        {
            context = new ApplicationDbContext();
        }
        public FacultyHubController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: FacultyHub
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult NewSponsorship()
        {
            var user = User.Identity;
            ApplicationBasicDetail application = new ApplicationBasicDetail();
            List<ApplicationViewModel> ApplicationList = application.GetApplicationListByFacultySponsorID(user.GetUserId(), "Pending");
            return View(ApplicationList);

        }

        public async Task<ActionResult> ApproveSponsor(string AppID)
        {
            ApplicationBasicDetail application = new ApplicationBasicDetail();
            application = await context.ApplicationBasicDetails.FindAsync(AppID);
            application.isFacultyApproved = true;
            context.Entry(application).State = System.Data.Entity.EntityState.Modified;
            await context.SaveChangesAsync();
            string to = "knigtdav001@gmail.com";
            string subject = "Approved";
            string body = "";
            await this.SendEmail(to, body, subject);
            return RedirectToAction("NewSponsorship");
        }

        public async Task SendEmail(string to, string body, string Subject)
        {
            GMailer mailer = new GMailer
            {
                ToEmail = to,
                Subject = Subject,
                Body = body,
                IsHtml = true
            };
            await mailer.Send();
        }


    }


}