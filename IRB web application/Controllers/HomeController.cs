﻿using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        ApplicationDbContext context = new ApplicationDbContext();
        UserDBRepository users = new UserDBRepository();

        public HomeController(ApplicationUserManager userManager)
        {                                                                     
            UserManager = userManager;
        }

        public HomeController()
        {
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!isAdminApproved())
                {
                    return RedirectToAction("NotConfirmed", "Account");
                }
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public PartialViewResult SuperAdminHome()
        {
            AdminHomeViewModel adminHome = new AdminHomeViewModel();
            var NumberofUni = context.universities.Select(c => c.ID).ToList();        
            int countUni = NumberofUni.Count();//Here you will  get count
            adminHome.NumberOfInstitution = countUni.ToString();

            var NumberofUsers = context.Users.Select(c => c.Id).ToList();
            int countUsers = NumberofUsers.Count();//Here you will  get count
            adminHome.NumberOfUsers = countUsers.ToString();
            return PartialView(adminHome);
        }

        public PartialViewResult StudentHome()
        {
            StudentHomeModel studentHome = new StudentHomeModel();
            studentHome.NumberOFApplications = "12";
            studentHome.NumberOfPendingApplications = "3";
            studentHome.NumberOfSubmittedApplications = "24";

            return PartialView(studentHome);
        }

        public PartialViewResult FacultyHome()
        {
            FacultyHomeViewModel facultytHome = new FacultyHomeViewModel();
            facultytHome.NumberOFApplications = "12";
            facultytHome.NumberOfPendingApplications = "3";
            facultytHome.NumberOfSubmittedApplications = "24";

            return PartialView(facultytHome);
        }

        public PartialViewResult StaffHome()
        {
            StaffHomeViewModel staffHome = new StaffHomeViewModel();
            staffHome.NumberOFApplications = "12";
            staffHome.NumberOfPendingApplications = "3";
            staffHome.NumberOfSubmittedApplications = "24";

            return PartialView(staffHome);
        }

        public PartialViewResult IRBMemberHome()
        {
            IRBMemberViewModel IRBMemberHome = new IRBMemberViewModel();
            IRBMemberHome.NumberOFApplications = "12";
            IRBMemberHome.NumberOfPendingApplications = "3";
            IRBMemberHome.NumberOfSubmittedApplications = "24";

            return PartialView(IRBMemberHome);
        }


        public PartialViewResult AdminHome()
        {
            AdminHomeViewModel UniadminHome = new AdminHomeViewModel();

            if (User.Identity.IsAuthenticated)
            {

                var Admin = User.Identity;
                var AdminID = Admin.GetUserId();
                var AdminUSer = context.Users.Find(AdminID);
                var NumberofUsers = users.GetUsersByUnversityID(AdminUSer.UniversityID);
                Session["UniversityID"] = AdminUSer.UniversityID;
                int countUsers = NumberofUsers.Count();//Here you will  get count
                UniadminHome.NumberOfUsers = countUsers.ToString();
            }
           
            return PartialView(UniadminHome);
        }
        public FileContentResult UserPhotos()
        {
            if (User.Identity.IsAuthenticated)
            {
                String userId = User.Identity.GetUserId();
                // to get the user details to load user Image
                var bdUsers = HttpContext.GetOwinContext().Get<ApplicationDbContext>();

                var userImage = bdUsers.Users.Where(x => x.Id == userId).FirstOrDefault();

                if (userId == null || userImage == null)
                {
                    string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                    byte[] imageData = null;
                    FileInfo fileInfo = new FileInfo(fileName);
                    long imageFileLength = fileInfo.Length;
                    FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                    BinaryReader br = new BinaryReader(fs);
                    imageData = br.ReadBytes((int)imageFileLength);
                    

                    return File(imageData, "image/png");

                }
                

                return new FileContentResult(userImage.UserPhoto, "image/jpeg");
            }
            else
            {
                string fileName = HttpContext.Server.MapPath(@"~/Images/noImg.png");

                byte[] imageData = null;
                FileInfo fileInfo = new FileInfo(fileName);
                long imageFileLength = fileInfo.Length;
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                imageData = br.ReadBytes((int)imageFileLength);
                return File(imageData, "image/png");

            }
        }

        public ActionResult About()
        {
            if (!isAdminApproved())
            {
                return RedirectToAction("NotConfirmed", "Account");
            }
            
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            if (!isAdminApproved())
            {
                return RedirectToAction("NotConfirmed", "Account");
            }
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public bool isAdminApproved()
        {
            bool t = false;
            try
            {
                var user = User.Identity;
                var presentUser = context.Users.Find(user.GetUserId());
               
                if (presentUser.AdminApproved)
                    return t = presentUser.AdminApproved;
                else
                    return t = false;
            }
            catch(Exception e)
            {
                
            }
            return t;
        }

        // send Email message
        public async Task sendEmail(string to, string body, string Subject)
        {
            GMailer mailer = new GMailer
            {
                ToEmail = to,
                Subject = Subject,
                Body = body,
                IsHtml = true
            };
            await mailer.Send();
        }
    }
}