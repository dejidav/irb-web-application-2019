﻿using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    public class IRBManagementController : Controller
    {

        ApplicationDbContext context;
        private ApplicationUserManager _userManager;

        public IRBManagementController()
        {
            context = new ApplicationDbContext();
        }

        public IRBManagementController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: IRBManagement
        public ActionResult Index()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());

            return View(presentUser);
        }

        public ActionResult ListIRBMembers()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            UserDBRepository Members = new UserDBRepository();

            return View(Members.GetAllIRBMembers(UniversityID));
        }

        public ActionResult Details(string Id)
        {
            ApplicationUser IRBMember = context.Users.Find(Id);
            return View(IRBMember);
        }

        public ActionResult ListAssignedApplications(string Id)
        {
            var IRBMember = context.Users.Find(Id);
            string IRBMemberEmail = IRBMember.Email;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetApplicationListByAssignedStaff(IRBMemberEmail);
            return View(ListApplications);
        }

        public ActionResult IRBApplicationsAssignedOrUndecided()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetListOFApplicationsByUniversityAndApplicationState(UniversityID, "Assigned");
            return View(ListApplications);
        }

        public ActionResult IRBSumbittedApplication()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            ApplicationBasicDetail Application = new ApplicationBasicDetail();
            List<ApplicationBasicDetail> ApplicationList = new List<ApplicationBasicDetail>();
            ApplicationList = Application.GetListOFNonStandardApplicationsByUniversityAndApplicationState(UniversityID, "Submitted");

            return View(ApplicationList);
        }

        public ActionResult AssignApplication(string Id)
        {
            Session["AssignAppID"] = Id;
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            applicationBasicDetail = context.ApplicationBasicDetails.Find(Id);

            UserDBRepository ListMembers = new UserDBRepository();
            ViewBag.Name = new SelectList(ListMembers.GetAllIRBMemberEmailAddress(UniversityID));
            return View(applicationBasicDetail);
        }

        [HttpPost]
        public ActionResult AssignToStaff(string Name)
        {

            var Id = Session["AssignAppID"].ToString();
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            UserDBRepository ListMembers = new UserDBRepository();
            ViewBag.Name = new SelectList(ListMembers.GetAllIRBMemberEmailAddress(UniversityID));

            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            applicationBasicDetail = context.ApplicationBasicDetails.Find(Id);
            applicationBasicDetail.AssignedStaff = Name;
            applicationBasicDetail.ApplicationState = "Assigned";
            applicationBasicDetail.DateStateUpdate = DateTime.Now;
            context.Entry(applicationBasicDetail).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();

            return RedirectToAction("IRBSumbittedApplication");
        }

        public ActionResult ListAllApplications()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetListOFSubmittedApplicationsByUniversity(UniversityID);
            return View(ListApplications);
        }

        public ActionResult ListIRBStandardApplication()
        {
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UniversityID = presentUser.UniversityID;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetListOFStandardApplicationsByUniversityAndApplicationState(UniversityID, "Submitted");
            return View(ListApplications);
        }


        public ActionResult ListApplicationsAssignedToMember()
        {
            
            ViewBag.State = new SelectList(new List<string> { "Assigned", "Approved", "Rejected", "Approved by Condition", "Reassigned"});
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UserEmail = presentUser.Email;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetApplicationListByAssignedStaffandState(UserEmail, "Assigned");
            return View(ListApplications);
        }

        [HttpPost]
        public ActionResult ListApplicationsAssignedToMember(string State)
        {
           if(State == "")
            {
                return RedirectToAction("ListApplicationsAssignedToMember");
            }
            ViewBag.State = new SelectList(new List<string> { "Assigned", "Approved", "Rejected", "Approved by Condition", "Reassigned" });
            var user = User.Identity;
            ApplicationUser presentUser = context.Users.Find(user.GetUserId());
            var UserEmail = presentUser.Email;
            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var ListApplications = applicationBasicDetail.GetApplicationListByAssignedStaffandState(UserEmail, State);
            return View(ListApplications);
        }


        public ActionResult Preview(string id)
        {
            string ApplicationID = id;
            if (id == null)
                ApplicationID = Session["ApplicationID"].ToString();

            ApplicationBasicDetail applicationBasicDetail = new ApplicationBasicDetail();
            var PI = applicationBasicDetail.GetPrincipalInvestigator(ApplicationID);
            ApplicationViewModel viewModel = new ApplicationViewModel
            {
                ApplicationBasicDetail = context.ApplicationBasicDetails.Find(ApplicationID),
                RerResr = context.RerResrs.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                ResearchStudyDetail = context.ResearchStudyDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                SubjectDefinedDetail = context.SubjectDefinedDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                RiskFactorDetail = context.RiskFactorDetails.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent1 = context.InformedConsent1s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent2 = context.InformedConsent2s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                InformedConsent3 = context.InformedConsent3s.FirstOrDefault(c => c.ApplicationID == ApplicationID),
                PrincipalInvestigator = PI,
               

            };
            return View(viewModel);
        }

        private static List<UploadFiles> GetFiles(string id)
        {
            List<UploadFiles> files = new List<UploadFiles>();
            UploadFiles file = new UploadFiles();
            files = file.GetUploadedFiles(id);
            return files;
        }

        [HttpPost]
        public FileResult DownloadFile(string fileId)
        {
            byte[] bytes;
            string fileName, contentType;
            UploadFiles FileToDownload = new UploadFiles();
            FileToDownload = FileToDownload.GetFIleToDownload(fileId);
            fileName = FileToDownload.Name;
            contentType = FileToDownload.ContentType;
            bytes = FileToDownload.Data;
            return File(bytes, contentType, fileName);
        }

        //Get
        public ActionResult StandardDecisions(string id)
        {
            StandardAppDecisions standardApp = new StandardAppDecisions();
            List<StandardAppDecisions> ListStandardApp = new List<StandardAppDecisions>();
            List<StandardDecisionViewModel> ListStandarddecisionViewmodel = new List<StandardDecisionViewModel>();
            StandardDecisionViewModel standardAppDecisions = new StandardDecisionViewModel();
            ListStandardApp = standardApp.GetStandardAppDecisions(id);
            foreach(var item in ListStandardApp)
            {
                var ReviewerLinq = from c in context.Users where c.Id == item.ReviewerID select c;
                ApplicationUser Reviewer = ReviewerLinq.FirstOrDefault();


               
                standardAppDecisions.ApplicationID = item.ApplicationID;
                standardAppDecisions.Comment = item.Comment;
                standardAppDecisions.DateSubmitted = item.DateSubmitted;
                standardAppDecisions.Decision = item.Decision;
                standardAppDecisions.ReviewerEmail = Reviewer.Email;
                standardAppDecisions.ReviewerName = Reviewer.FirstName + " " + Reviewer.LastName;
                ListStandarddecisionViewmodel.Add(standardAppDecisions);
            }


            return View(standardAppDecisions);
        }

    }
}