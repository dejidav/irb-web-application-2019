﻿using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    // not in use!!
    public class InstitutionController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Institution
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!isSuperAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Register()
        {
           
            return View();
        }

        public ActionResult Register(University model)
        {
            var university = new University { Name = model.Name, Location = model.Location, Url = model.Url, Website = model.Website };
            return View();
        }

        public Boolean isSuperAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ApplicationDbContext context = new ApplicationDbContext();
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "SuperAdmin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}