﻿using IRB_web_application.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Controllers
{
    public class usersController : Controller
    {

        ApplicationDbContext context;
        UserDBRepository UserDB = new UserDBRepository();
        private ApplicationUserManager _userManager;
        public usersController()
        {
            context = new ApplicationDbContext();
            
        }

        public usersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }



        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: users
        [Authorize]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

                ViewBag.displayMenu = "No";

                if (isSuperAdminUser() || isAdminUser())
                {
                    ViewBag.displayMenu = "Yes";
                }
                return View();
            }
            else
            {
                ViewBag.Name = "Not Logged IN";
            }
            return View();
        }

        public bool isAdminApproved()
        {
            var user = User.Identity;
            var presentUser = context.Users.Find(user.GetUserId());
            if (presentUser.AdminApproved)
                return presentUser.AdminApproved;
            else
                return false;
        }
        public ActionResult AdminList()
        {
            var universityID = Session["UniversityID"].ToString();
            ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Admin")).ToList(), "Name", "Name");
            return View(UserDB.GetUsersByUnversityID(universityID));
        }

        [HttpPost]
        public ActionResult AdminList(string UserType)
        {
            var universityID = Session["UniversityID"].ToString();
            ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Admin")).ToList(), "Name", "Name");
            if(UserType == "IRB Member")
            {
                return View(UserDB.GetIRBMemberUsersByUnversityID(universityID));

            }else if(UserType == "Student")
            {
                return View(UserDB.GetStudentUsersByUnversityID(universityID));
            }
            else if(UserType == "Staff")
            {
                return View(UserDB.GetStaffUsersByUnversityID(universityID));
            }
            else if (UserType == "Faculty")
            {
                return View(UserDB.GetFacultyUsersByUnversityID(universityID));
            }
            return View(UserDB.GetUsersByUnversityID(universityID));
        }

        public ActionResult NewUserDetail(string userId)
        {
            if (isSuperAdminUser() || isAdminUser())
            {
                var NewUser = context.Users.Find(userId);
                return View(NewUser);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult MakeIRBChair(string UserId)
        {
            ApplicationUser user = context.Users.FirstOrDefault(c => c.Id == UserId & c.UserType == "IRB Member");
            user.IsIRBChair = true;
            context.Entry(user).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
            return RedirectToAction("AdminList");
        }

        public ActionResult Approve(string userId)
        {
            Decision(userId, true);
            return RedirectToAction("Index", "Home");
        }

        public void Decision(string UserId, bool decision)
        {

            var NewUser = context.Users.Find(UserId);
            NewUser.AdminApproved = decision;
            context.SaveChanges();
        }

        public ActionResult Reject(string userId)
        {
            Decision(userId, false);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SuperAdminList()
        {
            return View(UserDB.GetAllUsersForSuperAdmin());
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            if (isSuperAdminUser())
            {
                ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Student") & !u.Name.Contains("IRB Member") & !u.Name.Contains("Staff") & !u.Name.Contains("Faculty"))
                                            .ToList(), "Name", "Name");
            }
            else
            {
                ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Admin")).ToList(), "Name", "Name");
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = context.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
           
            return View(user);
        }

        // POST: user/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,Gender,Email,UserName,UserType,StreetAddress1,StreetAddress2,City,State,Zip,CollegeName,department,ResearchArea,Campus,UserType")] ApplicationUser user)
        {
            if (isSuperAdminUser())
            {
                ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Student") & !u.Name.Contains("IRB Member") & !u.Name.Contains("Staff") & !u.Name.Contains("Faculty"))
                                            .ToList(), "Name", "Name");
            }
            else
            {
                ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("SuperAdmin") & !u.Name.Contains("Admin")).ToList(), "Name", "Name");
            }
            if (ModelState.IsValid)
            {
                user.UniversityID = Session["UniversityID"].ToString();
                this.UserManager.AddToRole(user.Id, user.UserType);
                context.Entry(user).State = EntityState.Modified; 
                context.SaveChanges();

                if (isAdminUser())
                    return RedirectToAction("AdminList");
                else
                    return RedirectToAction("SuperAdminList");
            }
            return View(user);
        }

        // GET: users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = context.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser user = context.Users.Find(id);
            this.UserManager.RemoveFromRole(user.Id, user.UserType);
            context.Users.Remove(user);
            context.SaveChanges();
            if (isAdminUser())
                return RedirectToAction("AdminList");
            else
                return RedirectToAction("SuperAdminList");
        }


        public Boolean isSuperAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                
                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "SuperAdmin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public Boolean isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;

                var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = UserManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
    }
}