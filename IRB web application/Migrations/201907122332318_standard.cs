namespace IRB_web_application.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class standard : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StandardAppDecisions",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                        ApplicationID = c.String(maxLength: 128),
                        ReviewerID = c.String(),
                        Comment = c.String(),
                        Decision = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ApplicationBasicDetail", t => t.ApplicationID)
                .Index(t => t.ApplicationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StandardAppDecisions", "ApplicationID", "dbo.ApplicationBasicDetail");
            DropIndex("dbo.StandardAppDecisions", new[] { "ApplicationID" });
            DropTable("dbo.StandardAppDecisions");
        }
    }
}
