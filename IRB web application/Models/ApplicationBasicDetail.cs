﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("ApplicationBasicDetail")]
    public class ApplicationBasicDetail
    {
        ApplicationDbContext db = new ApplicationDbContext();
        [Key]
        public string ApplicationID { get; set; }

        public string SubmittedApplicationID { get; set; }

        public bool isRerResr { get; set; }

        // Foreign key 
        public string UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual ApplicationUser PrincipalInvestigator { get; set; }

        [Required]
        [Display(Name = "Type of Application")]
        [StringLength(100)]
        public string TypeOfApplication { get; set; }

        [Required]
        [Display(Name = "Principal Investigator Signature")]
        [StringLength(100)]
        public string PISignedInitial { get; set; }

        [Required]
        [Display(Name = "Research Title")]
        [StringLength(100)]
        public string TitleOfResearch { get; set; }

        public int ApplicationIDint { get; set; }

        public string FacultyUserID { get; set; }

        public string FacultyName { get; set; }

        public string FacultyEmail { get; set; }

        public string FacultyPhoneNumber { get; set; }

        internal List<ApplicationBasicDetail> GetCollabApplicationListByState(object p)
        {
            throw new NotImplementedException();
        }

        public bool isFacultyApproved { get; set; }

        public bool isCollaborativeUsers { get; set; }

        [Display(Name = "Graduate Student?")]
        public bool isGraduateStudent { get; set; }

        public string isGraduateStudentOutput
        {
            get
            {
                return (bool)this.isGraduateStudent ? "Yes" : "No";
            }
        }

        public bool isCompleted { get; set; }

        public bool isSubmitted { get; set; }

        public DateTime? ApplicationStartDate { get; set; }

        public DateTime? LastEditedDate { get; set; }

        public DateTime? DateSubmitted { get; set; }

        public string AssignedStaff { get; set; }

        public string ApplicationState { get; set; }

        public DateTime? DateStateUpdate { get; set; }

        public bool GetisCompleted(string id)
        { 
            var query = from a in db.ApplicationBasicDetails
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

        public List<ApplicationBasicDetail> GetListOFSubmittedApplicationsByUniversity(string UniversityID)
        {
            var query2 = from a in db.Users
                         where a.UniversityID == UniversityID
                         select a.Id;

            List<ApplicationBasicDetail> List = new List<ApplicationBasicDetail>();
            List<string> UserIDs = new List<string>();
            UserIDs = query2.ToList();
            foreach(string d in UserIDs)
            {
                var query = from a in db.ApplicationBasicDetails
                            where a.UserID.Equals(d) & a.isSubmitted == true
                            select a;
                if(query.FirstOrDefault() != null)
                    List.Add(query.FirstOrDefault());
            }

            return List;
        }

        public List<ApplicationBasicDetail> GetApplicationListByAssignedStaff(string AssignedSTaff)
        {
            var query2 = from a in db.ApplicationBasicDetails
                         where a.AssignedStaff == AssignedSTaff
                         select a;
            return query2.ToList();
        }

        public List<ApplicationBasicDetail> GetApplicationListByAssignedStaffandState(string AssignedSTaff, string ApplicationState)
        {
            var query2 = from a in db.ApplicationBasicDetails
                         where a.AssignedStaff == AssignedSTaff & a.ApplicationState == ApplicationState
                         select a;
            return query2.ToList();
        }


        public List<ApplicationBasicDetail> GetListOFApplicationsByUniversityAndApplicationState(string UniversityID, string ApplicationState)
        {
            var query2 = from a in db.Users
                         where a.UniversityID == UniversityID
                         select a.Id;

            List<ApplicationBasicDetail> List = new List<ApplicationBasicDetail>();
            List<string> UserIDs = new List<string>();
            UserIDs = query2.ToList();
            foreach (string d in UserIDs)
            {
                var query = from a in db.ApplicationBasicDetails
                            where a.UserID.Equals(d) & a.isSubmitted == true & a.ApplicationState == ApplicationState
                            select a;
                if (query.FirstOrDefault() != null)
                    List.Add(query.FirstOrDefault());
            }

            return List;
        }

        public List<ApplicationBasicDetail> GetListOFStandardApplicationsByUniversityAndApplicationState(string UniversityID, string ApplicationState)
        {
            var query2 = from a in db.Users
                         where a.UniversityID == UniversityID
                         select a.Id;

            List<ApplicationBasicDetail> List = new List<ApplicationBasicDetail>();
            List<string> UserIDs = new List<string>();
            UserIDs = query2.ToList();
            foreach (string d in UserIDs)
            {
                var query = from a in db.ApplicationBasicDetails
                            where a.UserID.Equals(d) & a.isSubmitted == true & a.ApplicationState == ApplicationState & a.TypeOfApplication == "Standard"
                            select a;
                if (query.FirstOrDefault() != null)
                    List.Add(query.FirstOrDefault());
            }

            return List;
        }

        public List<ApplicationBasicDetail> GetListOFNonStandardApplicationsByUniversityAndApplicationState(string UniversityID, string ApplicationState)
        {
            var query2 = from a in db.Users
                         where a.UniversityID == UniversityID
                         select a.Id;

            List<ApplicationBasicDetail> List = new List<ApplicationBasicDetail>();
            List<string> UserIDs = new List<string>();
            UserIDs = query2.ToList();
            foreach (string d in UserIDs)
            {
                var query = from a in db.ApplicationBasicDetails
                            where a.UserID.Equals(d) & a.isSubmitted == true & a.ApplicationState == ApplicationState & a.TypeOfApplication != "Standard"
                            select a;
                if (query.FirstOrDefault() != null)
                    List.Add(query.FirstOrDefault());
            }

            return List;
        }


        public ApplicationUser GetPrincipalInvestigator(string ApplicationID)
        {
            var query = from a in db.ApplicationBasicDetails
                        where a.ApplicationID == ApplicationID
                        select a.UserID;

            string id = query.FirstOrDefault();

            var query2 = from a in db.Users
                         where a.Id == id
                         select a;

            return query2.FirstOrDefault();

        }

        public List<ApplicationViewModel> GetApplicationListByFacultySponsorID(string FacultyID, string ApplicationState)
        {
            ApplicationViewModel applicationViewModel = new ApplicationViewModel();
            List<ApplicationViewModel> applicationViewModels = new List<ApplicationViewModel>();
            var query2 = from a in db.ApplicationBasicDetails
                         where a.FacultyUserID == FacultyID & a.ApplicationState == ApplicationState
                         select a;

            foreach(var item in query2.ToList())
            {
                applicationViewModel.ApplicationBasicDetail = item;
                var query = from a in db.Users
                            where a.Id == item.UserID
                            select a;
                applicationViewModel.PrincipalInvestigator = query.FirstOrDefault();
                applicationViewModels.Add(applicationViewModel);
            }



            return applicationViewModels;
        }

        public List<dynamic> GetApplicationListByFacultySponsorEmail(string FacultyEmail, string ApplicationState)
        {
            SponsorListViewModel sponsorListViewModel = new SponsorListViewModel();
            dynamic query2 = from a in db.ApplicationBasicDetails
                         join b in db.Users on a.UserID equals b.Id
                         where a.FacultyEmail == FacultyEmail & a.ApplicationState == ApplicationState
                         select new
                         {
                             a.ApplicationID,
                             b.FirstName,
                             b.LastName,
                             a.TypeOfApplication,
                             a.TitleOfResearch,
                             a.isFacultyApproved,
                             a.ApplicationStartDate,
                             a.DateSubmitted

                         };


            foreach(var item in query2.ToList())
            {
                sponsorListViewModel.ApplicationID = item.ApplicationID;
                sponsorListViewModel.FirstName = item.FirstName;
                sponsorListViewModel.LastName = item.LastName;
            }

            return query2.ToList();
        }


        public List<ApplicationBasicDetail> GetCollabApplicationListByState(string UserID, string ApplicationState)
        {
            List<ApplicationBasicDetail> AppList = new List<ApplicationBasicDetail>();
            List<ApplicationBasicDetail> AllList = new List<ApplicationBasicDetail>();
            AllList = GetAllCollabApplicationList(UserID);
            foreach(var item in AllList)
            {
                if( item.ApplicationState == ApplicationState)
                {
                    AppList.Add(item);
                }
            }

            return AppList;
        }

        public List<ApplicationBasicDetail> GetPICollabAppList(string UserID)
        {
            var query = from a in db.ApplicationBasicDetails
                        where a.UserID == UserID
                        select a;
            List<ApplicationBasicDetail> AppList = new List<ApplicationBasicDetail>();
            AppList = query.ToList();
            foreach(var item in AppList)
            {
                if (item.isCollaborativeUsers)
                {
                    AppList.Add(item);
                }
            }
            return AppList;

            
        }

        public List<ApplicationBasicDetail> GetAllCollabApplicationList(string UserID)
        {
            var query2 = from a in db.CollaborativeUsers
                         where a.IRBUserID == UserID
                         select a.ApplicationID;

            List<string> AppIDs = new List<string>();

            AppIDs = query2.ToList();

          

            List<ApplicationBasicDetail> AppList = new List<ApplicationBasicDetail>();

            if (GetPICollabAppList(UserID) != null)
            {
                foreach(var item in GetPICollabAppList(UserID))
                {
                    AppList.Add(item);
                }
            }
            foreach (var item in AppIDs)
            {
                var query = from a in db.ApplicationBasicDetails
                            where a.ApplicationID == item 
                            select a;
                AppList.Add(query.FirstOrDefault());

            }


            return AppList;
        }

        public List<ApplicationUser> GetListOfCollabPIs(string UserID)
        {
            List<ApplicationBasicDetail> applicationBasicDetails = new List<ApplicationBasicDetail>();
            applicationBasicDetails = GetAllCollabApplicationList(UserID);
            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach (var item in applicationBasicDetails)
            {
                var query = from a in db.Users
                            where a.Id == item.UserID
                            select a;
                users.Add(query.FirstOrDefault());


            }
            return users;
        }

        public List<ApplicationBasicDetail> GetListOfCollabApplicationsByPIID(string Id, string UserID)
        {
            List<ApplicationBasicDetail> AppList = new List<ApplicationBasicDetail>();
            List<ApplicationBasicDetail> AllList = new List<ApplicationBasicDetail>();
            AllList = GetAllCollabApplicationList(UserID);
            foreach (var item in AllList)
            {
                if (item.UserID == Id)
                {
                    AppList.Add(item);
                }
            }
            return AllList;
        }


        //[EnumDataType(typeof(State))]
        //public State ApplicationState { get; set; }

        //public enum State
        //{
        //    Pending = 0,
        //    Submitted = 1,
        //    UnderReview = 2,
        //    Approved = 3,
        //    Reassigned = 4,
        //    ApprovedWithRecommendation = 5,
        //    Rejected = 6
        //}

    }

    public class SponsorListViewModel
    {
        public string ApplicationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TypeOfApplication { get; set; }
        public string TitleOfResearch { get; set; }
        public string isFacultyApproved { get; set; }
        public string ApplicationStartDate { get; set; }
        public string DateSubmitted { get; set; }
    }
}