﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    public class ApplicationViewModel
    {
        ApplicationDbContext db = new ApplicationDbContext();
       
        public ApplicationUser PrincipalInvestigator { get; set; }
        public UploadFiles UploadFiles { get; set; }
        public ApplicationBasicDetail ApplicationBasicDetail { get; set; }
        public RerResr RerResr { get; set; }
        public ResearchStudyDetail ResearchStudyDetail { get; set; }
        public SubjectDefinedDetail SubjectDefinedDetail { get; set; }
        public RiskFactorDetail RiskFactorDetail { get; set; }
        public InformedConsent1 InformedConsent1 { get; set; }
        public InformedConsent2 InformedConsent2 { get; set; }
        public InformedConsent3 InformedConsent3 { get; set; }

       

    }
}