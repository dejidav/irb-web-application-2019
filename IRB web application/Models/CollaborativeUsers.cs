﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("CollaborativeUsers")]
    public class CollaborativeUsers
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        public string EmailAddress { get; set; }

        public string FullName { get; set; }

        public string IRBUserID { get; set; }

        public bool isRegistered { get; set; }


        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisRegistered(string id)
        {
            var query = from a in db.CollaborativeUsers
                        where a.ApplicationID.Equals(id)
                        select a.isRegistered;
            return query.FirstOrDefault();
        }


        public List<CollaborativeUsers> GetCollabUsers(string id)
        {
            var query = from a in db.CollaborativeUsers
                        where a.ApplicationID.Equals(id)
                        select a;
            return query.ToList();
        }
    }
}