﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("DataHandlingDetails")]
    public class DataHandlingDetail
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        [Display(Name = "Data Collection Method")]
        [StringLength(5000)]
        public string Data_Collection_Method { get; set; }

        [Display(Name = "Data Collection Method Other")]
        [StringLength(5000)]
        public string Data_Collection_Method_Other { get; set; }

        [Display(Name = "Data Collection Identifiers")]       
        public bool Data_Collect_Identi { get; set; }

        public string IsCollectIdentifiers
        {
            get
            {
                return (bool)this.Data_Collect_Identi ? "Yes" : "No";
            }
        }

        [Display(Name = "Data Retain Identifiers for Analysis")]
        public bool Data_Retain_Identi_Analysis { get; set; }

        public string IsCollectIdentifiersAnalysis
        {
            get
            {
                return (bool)this.Data_Retain_Identi_Analysis ? "Yes" : "No";
            }
        }

        [Display(Name = "Data Retain Identifiers for Reporting")]
        public bool Data_Retain_Identi_Reporting { get; set; }

        public string IsCollectIdentifiersReporting
        {
            get
            {
                return (bool)this.Data_Retain_Identi_Reporting ? "Yes" : "No";
            }
        }

        [Display(Name = "Research Disseminated to subjects")]
        [StringLength(5000)]
        public string Research_Dissem_Sub { get; set; }

        [Display(Name = "Storage Arrangement")]
        [StringLength(5000)]
        public string Storage_Arrangement { get; set; }

        [Display(Name = "Accounting Person")]
        [StringLength(5000)]
        public string Acc_Person { get; set; }

        [Display(Name = "Schedule and Method for De-Identification Of Data")]
        [StringLength(5000)]
        public string sch_mthd_deidenti_data { get; set; }

        [Display(Name = " Schedule And Methods For Long-Term Storage Of Raw Data.")]
        [StringLength(5000)]
        public string sch_mthd_raw_data { get; set; }

        [Display(Name = "Schedule And Methods For Eventual Destruction Of Data.")]
        [StringLength(5000)]
        public string sch_mthd_even_destruct { get; set; }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        public bool isCompleted { get; set; }

        public DateTime? ApplicationStartDate { get; set; }

        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.DataHandlingDetails
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

    }
}