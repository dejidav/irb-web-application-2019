﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    public class FacultyHomeViewModel
    {
        public string NumberOFApplications { get; set; }
        public string NumberOfSubmittedApplications { get; set; }
        public string NumberOfPendingApplications { get; set; }
    }
}