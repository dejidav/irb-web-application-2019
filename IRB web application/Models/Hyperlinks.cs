﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Models
{
    [Table("Hyperlinks")]
    public class Hyperlinks
    {
        ApplicationDbContext db = new ApplicationDbContext();
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public List<Hyperlinks> GetHyperlinks(string id)
        {
            var query = from a in db.Hyperlinks
                        where a.ApplicationID.Equals(id)
                        select a;
            return query.ToList();
        }
    }

    public static class LinkHelper
    {
        public static string ExternalLink(this HtmlHelper helper, string url, string text)
        {
            return String.Format("<a href='http://{0}' target=\"_blank\">{1}</a>", url, text);
        }
    }
}

