﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IRB_web_application.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public bool AdminApproved { get; set; }
        public byte[] UserPhoto { get; set; }
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [UIHint("MaleFemale")]
        [Display(Name = "Gender")]
        [BooleanDisplayValuesAsMaleFemaleAttribute]
        public bool Gender { get; set; }
        public string ISMale
        {
            get
            {
                return (bool)this.Gender ? "Female" : "Male";
            }
        }
        [Required]
        [Display(Name = "Street Address")]
        public string StreetAddress1 { get; set; }

        [Display(Name = "Street Address 2")]
        public string StreetAddress2 { get; set; }

        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required]
        [Display(Name = "Zip")]
        [StringLength(5, ErrorMessage = "The {0} must be {2} characters long.", MinimumLength = 5)]
        public string Zip { get; set; }

        [Required]
        [Display(Name = "College Name")]
        public string CollegeName { get; set; }
        [Required]
        [Display(Name = "Department")]
        public string department { get; set; }
        [Required]
        [Display(Name = "Research Area")]
        public string ResearchArea { get; set; }
        [Required]
        [Display(Name = "Campus")]
        public string Campus { get; set; }
        [Required]
        public string UserType { get; set; }

        public bool IsIRBChair { get; set; }

        // Foreign key 

        public string UniversityID { get; set; }

        [ForeignKey("UniversityID")]
        public virtual University University { get; set; }

      


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {

            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<University> universities { get; set; }
        public DbSet<ApplicationBasicDetail> ApplicationBasicDetails { get; set; }

        public DbSet<ResearchStudyDetail> ResearchStudyDetails { get; set; }

        public DbSet<SubjectDefinedDetail> SubjectDefinedDetails { get; set; }

        public DbSet<DataHandlingDetail> DataHandlingDetails { get; set; }

        public DbSet<RiskFactorDetail> RiskFactorDetails { get; set; }

        public DbSet<RerResr> RerResrs { get; set; }

        public DbSet<InformedConsent1> InformedConsent1s { get; set; }

        public DbSet<InformedConsent2> InformedConsent2s { get; set; }

        public DbSet<InformedConsent3> InformedConsent3s { get; set; }

        public DbSet<UploadFiles> UploadFiles { get; set; }

        public DbSet<Hyperlinks> Hyperlinks { get; set; }

        public DbSet<StandardAppDecisions> StandardAppDecisions { get; set; }
        
        public DbSet<CollaborativeUsers> CollaborativeUsers { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public class ApplicationBasicDetailContext : DbContext
        {
            DbSet<ApplicationBasicDetail> ApplicationBasicDetails { get; set; }
         }

        public class UploadFilesContext : DbContext
        {
            DbSet<UploadFiles> UploadFiles { get; set; }
        }



        public class ResearchStudyDetailContext : DbContext
        {
            DbSet<ResearchStudyDetail> ResearchStudyDetails { get; set; }
        }

        public class RiskFactorDetailContext : DbContext
        {
            DbSet<ResearchStudyDetail> RiskFactorDetails { get; set; }
        }

        public class DataHandlingDetailContext : DbContext
        {
            DbSet<DataHandlingDetail> DataHandlingDetails { get; set; }
        }


        public class SubjectDefinedDetailContext : DbContext
        {
            DbSet<SubjectDefinedDetail> SubjectDefinedDetails { get; set; }
        }

        public class RerResrContext : DbContext
        {
            DbSet<RerResr> RerResrDetails { get; set; }
        }

        public class StandardAppDecisionsContext : DbContext
        {
            DbSet<StandardAppDecisions> StandardAppDecisions { get; set; }
        }


    }
}