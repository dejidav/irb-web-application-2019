﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("InformedConsent1")]
    public class InformedConsent1
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        [Display(Name = "Type of Consent")]
        [StringLength(5000)]
        public string TypeOfConsent { get; set; }

        [Display(Name = "Consent Form Distri")]
        [StringLength(5000)]
        public string ConsentFormDistri { get; set; }
        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        public bool IsSubInfWithdraw { get; set; }

        public string IsSubInfWithdrawString
        {
            get
            {
                return (bool)this.IsSubInfWithdraw ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoSubInfWithdraw { get; set; }


        public bool IsWrittenConsent { get; set; }

        public string IsWrittenConsentString
        {
            get
            {
                return (bool)this.IsWrittenConsent ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoWrittenConsent { get; set; }


        public bool IsOralConsent { get; set; }

        public string IsOralConsentString
        {
            get
            {
                return (bool)this.IsOralConsent ? "Yes" : "No";
            }
        }

        [Display(Name = "If Yes, Explain")]
        [StringLength(5000)]
        public string YesOralConsent { get; set; }

        public bool IsMinorParticipate { get; set; }

        public string IsMinorParticipateString
        {
            get
            {
                return (bool)this.IsMinorParticipate ? "Yes" : "No";
            }
        }

        [Display(Name = "If Yes, Explain")]
        [StringLength(5000)]
        public string YesMinorParticipate { get; set; }

        public bool IsMinorSubObtainAssent { get; set; }

        public string IsMinorSubObtainAssentString
        {
            get
            {
                return (bool)this.IsMinorSubObtainAssent ? "Yes" : "No";
            }
        }

        [Display(Name = "If Yes, Explain")]
        [StringLength(5000)]
        public string YesMinorSubObtainAssent { get; set; }

        public bool IsPrinciConsentWaive { get; set; }

        public string IsPrinciConsentWaiveString
        {
            get
            {
                return (bool)this.IsPrinciConsentWaive ? "Yes" : "No";
            }
        }

        [Display(Name = "If Yes, Explain")]
        [StringLength(5000)]
        public string YesPrinciConsentWaive { get; set; }

        public bool isCompleted { get; set; }



        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.InformedConsent1s
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }
    }
}