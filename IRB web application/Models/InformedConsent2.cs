﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("InformedConsent2")]
    public class InformedConsent2
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }


        public bool IsR01 { get; set; }

        public string IsR01String
        {
            get
            {
                return (bool)this.IsR01 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR01 { get; set; }

        public bool IsR02 { get; set; }

        public string IsR02String
        {
            get
            {
                return (bool)this.IsR02 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR02 { get; set; }

        public bool IsR03 { get; set; }

        public string IsR03String
        {
            get
            {
                return (bool)this.IsR03 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR03 { get; set; }

        public bool IsR04 { get; set; }

        public string IsR04String
        {
            get
            {
                return (bool)this.IsR04 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR04 { get; set; }

        public bool IsR05 { get; set; }

        public string IsR05String
        {
            get
            {
                return (bool)this.IsR05 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR05 { get; set; }

        public bool IsR06 { get; set; }

        public string IsR06String
        {
            get
            {
                return (bool)this.IsR06 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR06 { get; set; }

        public bool IsR07 { get; set; }

        public string IsR07String
        {
            get
            {
                return (bool)this.IsR07 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR07 { get; set; }

        public bool IsR08 { get; set; }

        public string IsR08String
        {
            get
            {
                return (bool)this.IsR08 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR08 { get; set; }

        public bool IsR09 { get; set; }

        public string IsR09String
        {
            get
            {
                return (bool)this.IsR09 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR09 { get; set; }

        public bool IsR10 { get; set; }

        public string IsR10String
        {
            get
            {
                return (bool)this.IsR10 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR10 { get; set; }

        public bool IsR11 { get; set; }

        public string IsR11String
        {
            get
            {
                return (bool)this.IsR11 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR11 { get; set; }

        public bool IsR12 { get; set; }

        public string IsR12String
        {
            get
            {
                return (bool)this.IsR12 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR12 { get; set; }

        public bool IsR13 { get; set; }

        public string IsR13String
        {
            get
            {
                return (bool)this.IsR13 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR13 { get; set; }

        public bool IsR14 { get; set; }

        public string IsR14String
        {
            get
            {
                return (bool)this.IsR14 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR14 { get; set; }

        public bool IsR15 { get; set; }

        public string IsR15String
        {
            get
            {
                return (bool)this.IsR15 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR15 { get; set; }

        public bool IsR16 { get; set; }

        public string IsR16String
        {
            get
            {
                return (bool)this.IsR16 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR16 { get; set; }

        public bool IsR17 { get; set; }

        public string IsR17String
        {
            get
            {
                return (bool)this.IsR17 ? "Yes" : "No";
            }
        }

        [Display(Name = "If No, Explain")]
        [StringLength(5000)]
        public string NoR17 { get; set; }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }
        public bool isCompleted { get; set; }



        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.InformedConsent2s
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

    }
}