﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("InformedConsent3")]
    public class InformedConsent3
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        public bool IsR18 { get; set; }

        public string IsR18String
        {
            get
            {
                return (bool)this.IsR18 ? "Yes" : "No";
            }
        }
        public bool IsR19 { get; set; }

        public string IsR19String
        {
            get
            {
                return (bool)this.IsR19 ? "Yes" : "No";
            }
        }

        public bool IsR20 { get; set; }

        public string IsR20String
        {
            get
            {
                return (bool)this.IsR20 ? "Yes" : "No";
            }
        }

        public bool IsR21 { get; set; }

        public string IsR21String
        {
            get
            {
                return (bool)this.IsR21 ? "Yes" : "No";
            }
        }


        public bool IsR22 { get; set; }

        public string IsR22String
        {
            get
            {
                return (bool)this.IsR22 ? "Yes" : "No";
            }
        }


        public bool IsR23 { get; set; }

        public string IsR23String
        {
            get
            {
                return (bool)this.IsR23 ? "Yes" : "No";
            }
        }


        public bool IsR24 { get; set; }

        public string IsR24String
        {
            get
            {
                return (bool)this.IsR24 ? "Yes" : "No";
            }
        }


        public bool IsR25 { get; set; }

        public string IsR25String
        {
            get
            {
                return (bool)this.IsR25 ? "Yes" : "No";
            }
        }


        public bool IsR26 { get; set; }

        public string IsR26String
        {
            get
            {
                return (bool)this.IsR26 ? "Yes" : "No";
            }
        }


        public bool IsR27 { get; set; }

        public string IsR27String
        {
            get
            {
                return (bool)this.IsR27 ? "Yes" : "No";
            }
        }


        public bool IsR28 { get; set; }

        public string IsR28String
        {
            get
            {
                return (bool)this.IsR28 ? "Yes" : "No";
            }
        }


        public bool IsR29 { get; set; }

        public string IsR29String
        {
            get
            {
                return (bool)this.IsR29 ? "Yes" : "No";
            }
        }


        public bool IsR30 { get; set; }

        public string IsR30String
        {
            get
            {
                return (bool)this.IsR30 ? "Yes" : "No";
            }
        }


        public bool IsR31 { get; set; }

        public string IsR31String
        {
            get
            {
                return (bool)this.IsR31 ? "Yes" : "No";
            }
        }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        public bool isCompleted { get; set; }



        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.InformedConsent3s
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

    }
}