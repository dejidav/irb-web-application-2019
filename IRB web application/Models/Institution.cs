﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRB_web_application.Models
{
    interface Institution
    {
         string Name { get; set; }
         string Location { get; set; }
         string Url { get; set; }
         string Website { get; set; }
         string ID { get; set; }
         string Type { get; set; } 
    }
}
