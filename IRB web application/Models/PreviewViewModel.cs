﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    public class PreviewViewModel
    {
        public ApplicationUser User { get; set; }
        public ApplicationBasicDetail BasicDetail { get; set; }
        public ResearchStudyDetail ResearchStudyDetail { get; set; }
        public DataHandlingDetail DataHandlingDetail { get; set; }
        public InformedConsent1 InformedConsent1 { get; set; }
        public InformedConsent2 InformedConsent2 { get; set; }
        public InformedConsent3 InformedConsent3 { get; set; }
        public RiskFactorDetail RiskFactorDetail { get; set; }
        public RerResr RerResr { get; set; }
        public SubjectDefinedDetail SubjectDefinedDetail { get; set; }
    }
}