﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("RerResr")]
    public class RerResr
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        [Display(Name = "Que 1")]
        [StringLength(5000)]
        public string Que1 { get; set; }

        [Display(Name = "Que 2")]
        [StringLength(5000)]
        public string Que2 { get; set; }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        public bool isCompleted { get; set; }



        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.RerResrs
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }
    }
}