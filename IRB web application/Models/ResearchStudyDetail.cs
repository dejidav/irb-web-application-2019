﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("ResearchStudyDetail")]
    public class ResearchStudyDetail
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

       
        [Display(Name = "Research Study Proposal")]
        [StringLength(5000)]
        public string Research_std_Proposal { get; set; }


        [Display(Name = "Fund Agency")]
        [StringLength(5000)]
        public string Fund_Agency { get; set; }


        [Display(Name = "IRB Approve Request")]
        [StringLength(5000)]
        public string IRB_appr_req { get; set; }

        [Display(Name = "Data Start Date")]      
        public DateTime? Data_Start_date { get; set; }

        [Display(Name = "Data End Date")]
        public DateTime? Data_End_Date { get; set; }

        [Display(Name = "Project Study Duration")]
        [StringLength(5000)]
        public string Project_Study_Duration { get; set; }

        [Display(Name = "Purpose")]
        [StringLength(5000)]
        public string Purpose { get; set; }

        [Display(Name = "Methodology")]
        [StringLength(5000)]
        public string Methodology { get; set; }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        public bool isCompleted { get; set; }

       

        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.ResearchStudyDetails
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }


    }
}