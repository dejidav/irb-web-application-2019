﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Models
{
    public class ResearchStudyDetailViewModel
    {
        public ResearchStudyDetail ResearchStudyDetail { get; set; }
        public List<SelectListItem> ResearchStudyProposal { get; set; }
    }
}