﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("RiskFactorDetails")]
    public class RiskFactorDetail
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        [Display(Name = "With Respect To Any Of The Above Criteria, Subjects Are At Risk. ")]
        public bool Sub_Risk { get; set; }

        public string IsSubRisk
        {
            get
            {
                return (bool)this.Sub_Risk ? "Yes" : "No";
            }
        }

        [Display(Name = "Experimental Drugs Will Be Used.")]
        public bool Exp_Drug { get; set; }

        public string IsExpDrug
        {
            get
            {
                return (bool)this.Exp_Drug ? "Yes" : "No";
            }
        }

        [Display(Name = "Potential For A Medical Problem Exists. ")]
        public bool Medical_Prob { get; set; }

        public string IsMedical_Prob
        {
            get
            {
                return (bool)this.Medical_Prob ? "Yes" : "No";
            }
        }


        [Display(Name = "Subjects May Experience Physical Discomfort Greater Than In Everyday Life.")]
        public bool sub_phys_discomfort { get; set; }

        public string Issub_phys_discomfort
        {
            get
            {
                return (bool)this.sub_phys_discomfort ? "Yes" : "No";
            }
        }


        [Display(Name = "Subjects May Experience Mental Discomfort Greater Than In Everyday Life")]
        public bool sub_mental_discomfort { get; set; }

        public string Issub_mental_discomfort
        {
            get
            {
                return (bool)this.sub_mental_discomfort ? "Yes" : "No";
            }
        }

        [Display(Name = "Electrical Equipment Will Be Used")]
        public bool elec_equip { get; set; }

        public string Iselec_equip
        {
            get
            {
                return (bool)this.elec_equip ? "Yes" : "No";
            }
        }

        [Display(Name = "Subjects Will Be Tape Recorded, Photographed, Or Videotaped.")]
        public bool sub_record { get; set; }

        public string Issub_record
        {
            get
            {
                return (bool)this.sub_record ? "Yes" : "No";
            }
        }

        [Display(Name = " Does Any Part Of This Activity Have The Potential For Coercion Of The Subject? ")]
        public bool coercion_sub { get; set; }

        public string Iscoercion_sub
        {
            get
            {
                return (bool)this.coercion_sub ? "Yes" : "No";
            }
        }

        [Display(Name = "Are The Subjects Exposed To Deception?")]
        public bool sub_deception { get; set; }

        public string Issub_deception
        {
            get
            {
                return (bool)this.sub_deception ? "Yes" : "No";
            }
        }


        [Display(Name = "If Yes To Either Question, Explain Why Coercion Or Deception Is Necessary ")]
        [StringLength(5000)]
        public string exp_dtl { get; set; }

        [Display(Name = "What Possible Benefits, If Any? ")]
        [StringLength(5000)]
        public string Possible_Benefits { get; set; }

        [Display(Name = " What Contributions To General Knowledge In The Field Of Inquiry Or Possible Benefits Could Be Derived From The Research? ")]
        [StringLength(5000)]
        public string Contri_To_Gk { get; set; }

        [Display(Name = "If Subjects Will Be Rewarded Or Compensated, State The Amount, Types, And Timetable For Compensation.")]
        [StringLength(5000)]
        public string Sub_Rewarded_Compen { get; set; }
    
        [StringLength(5000)]
        public string Field43 { get; set; }
 
        [StringLength(5000)]
        public string Field44 { get; set; }

        [StringLength(5000)]
        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public bool isCompleted { get; set; }

       

        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.RiskFactorDetails
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }
    }
}