﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("StandardAppDecisions")]
    public class StandardAppDecisions
    {
        ApplicationDbContext db = new ApplicationDbContext();
        [Key]
        public string ID { get; set; }
        // Foreign key 
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        public string ReviewerID { get; set; }
        public string Comment { get; set; }

        public string Decision { get; set; }

        public DateTime? DateSubmitted { get; set; }

        public List<StandardAppDecisions> GetStandardAppDecisions(string ApplicationID)
        {
            var query = from a in db.StandardAppDecisions
                        where a.ApplicationID == ApplicationID
                        select a;


            return query.ToList();
        }

    }
}