﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    public class StandardDecisionViewModel
    {
        public string ApplicationID { get; set; }
        
        public string ReviewerName { get; set; }      
        
        public string ReviewerEmail { get; set; }

        public DateTime? DateSubmitted { get; set; }

        public string Comment { get; set; }

        public string Decision { get; set; }


    }
}