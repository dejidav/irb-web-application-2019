﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("SubjectDefinedDetails")]
    public class SubjectDefinedDetail
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        [Display(Name = "Research Subjects")]
        [StringLength(5000)]
        public string Research_sub { get; set; }

        [Display(Name = "Research Subject Population")]
        [StringLength(5000)]
        public string Research_sub_population { get; set; }

        [Display(Name = "Safeguard")]
        [StringLength(5000)]
        public string Safeguard { get; set; }

        [Display(Name = "Approx Subjects recruited")]
        [StringLength(5000)]
        public string Approx_sub_recruited { get; set; }

        [Display(Name = "Sub Recruited")]
        [StringLength(5000)]
        public string Sub_recruited { get; set; }

        [Display(Name = "Notes")]
        [StringLength(5000)]
        public string Notes { get; set; }

        [Display(Name = "Research Sub population Other")]
        [StringLength(5000)]
        public string Research_Sub_Population_Other { get; set; }

        [Display(Name = "Sub Recruited other")]
        [StringLength(5000)]
        public string Sub_Recruited_Other { get; set; }

        public bool isCompleted { get; set; }

      

        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.SubjectDefinedDetails
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

    }
}