﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IRB_web_application.Models
{
    public class SubjectDefinedViewModel
    {
        public SubjectDefinedDetail SubjectDefinedDetail { get; set; }
        public List<SelectListItem> someList { get; set; }
    }
}