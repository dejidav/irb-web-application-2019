﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("universities")]
    public class University 
    {
        ApplicationDbContext db = new ApplicationDbContext();

        [Required]
        [Display(Name = "Name of Institution")]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Address")]
        [StringLength(255)]
        public string Location { get; set; }
        [Required]
        [Display(Name = "subdomain URL")]
        [StringLength(100)]
        public string Url { get; set; }
        [Required]
        [Display(Name = "Website")]
        [StringLength(100)]
        public string Website { get; set; }

        [Key]
        public string ID { get; set; }
        
       

        public string GetNameById(string Id)
        {
            var query = from a in db.universities
                        where a.ID == Id
                        select a.Name;

            return query.FirstOrDefault();
        }


        public University GetUniversityById(string Id)
        {
            var query = from a in db.universities
                        where a.ID.Equals(Id)
                        select a;

            return query.FirstOrDefault();
        }

        public List<string> GetAllUniversitiesName()
        {
            var query = from a in db.universities
                        select a.Name;
            return query.ToList();
        }

        public string GetUniIDFromName(string Name)
        {
            var query = from a in db.universities
                        where a.Name.Equals(Name)
                        select a.ID;
            return query.FirstOrDefault();
        }

        public List<string> GetAllUniversityAdminEmail(string Id)
        {
            var query = from a in db.Users
                        where a.UniversityID.Equals(Id) & a.UserType.Equals("Admin")                     
                        select a.Email;
            return query.ToList();
        }
    }
}