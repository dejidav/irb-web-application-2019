﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    [Table("UploadFiles")]
    public class UploadFiles
    {
        [Key]
        public string ID { get; set; }

        //Foreign Key
        public string ApplicationID { get; set; }

        [ForeignKey("ApplicationID")]
        public virtual ApplicationBasicDetail Application { get; set; }

        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }

        

        public bool isCompleted { get; set; }

        public DateTime? LastEditedDate { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool GetisCompleted(string id)
        {
            var query = from a in db.UploadFiles
                        where a.ApplicationID.Equals(id)
                        select a.isCompleted;
            return query.FirstOrDefault();
        }

        
        public List<UploadFiles> GetUploadedFiles(string id)
        {
            var query = from a in db.UploadFiles
                        where a.ApplicationID.Equals(id)
                        select a;
            return query.ToList();
        }

        public UploadFiles GetFIleToDownload(string id)
        {
            var query = from a in db.UploadFiles
                        where a.ID.Equals(id)
                        select a;
            return query.FirstOrDefault();
        }
    }
}