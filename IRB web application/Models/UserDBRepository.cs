﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IRB_web_application.Models
{
    public class UserDBRepository
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public List<ApplicationUser> GetUsersByUnversityID(string universityID)
        {
            var query = from usersList in context.Users
                        where usersList.UniversityID == universityID
                        select usersList;

            return query.ToList();
        }

        public List<ApplicationUser> GetIRBMemberUsersByUnversityID(string universityID)
        {
            var query = from usersList in context.Users
                        where usersList.UniversityID == universityID & usersList.UserType == "IRB Member"
                        select usersList;

            return query.ToList();
        }

        public List<ApplicationUser> GetStudentUsersByUnversityID(string universityID)
        {
            var query = from usersList in context.Users
                        where usersList.UniversityID == universityID & usersList.UserType == "Student"
                        select usersList;

            return query.ToList();
        }

        public List<ApplicationUser> GetStaffUsersByUnversityID(string universityID)
        {
            var query = from usersList in context.Users
                        where usersList.UniversityID == universityID & usersList.UserType == "Staff"
                        select usersList;

            return query.ToList();
        }

        public List<ApplicationUser> GetFacultyUsersByUnversityID(string universityID)
        {
            var query = from usersList in context.Users
                        where usersList.UniversityID == universityID & usersList.UserType == "Faculty"
                        select usersList;

            return query.ToList();
        }



        public List<ApplicationUser> GetAllUsersForSuperAdmin()
        {
            var query = from usersList in context.Users
                        select usersList;

            return query.ToList();
        }

        public List<ApplicationUser> GetAllIRBMembers(string UniversityID)
        {
            var query = from usersList in context.Users
                        where usersList.UserType == "IRB Member" & usersList.UniversityID == UniversityID
                        select usersList;

            return query.ToList();
        }

        public List<string> GetAllIRBMemberEmailAddress(string UniversityID)
        {
            List<ApplicationUser> members = new List<ApplicationUser>();
            members = GetAllIRBMembers(UniversityID);
            List<string> Emails = new List<string>();
            foreach(var m in members)
            {
                Emails.Add(m.Email);
            }
            return Emails;
        }

       


    }
}